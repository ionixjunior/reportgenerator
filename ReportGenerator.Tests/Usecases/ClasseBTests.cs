﻿using System;
using NUnit.Framework;
using ReportGenerator.Usecases;

namespace ReportGenerator.Tests.Usecases
{
    [TestFixture(Category = "TestesB")]
    public class ClasseBTests
    {
        private ClasseB _classe;

        [SetUp]
        public void SetUp()
        {
            _classe = new ClasseB();
        }

        [Test]
        public void Validar()
        {
            Assert.IsTrue(_classe.Executar());
        }
    }
}
