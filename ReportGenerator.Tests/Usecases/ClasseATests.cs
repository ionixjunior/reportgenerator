﻿using System;
using NUnit.Framework;
using ReportGenerator.Usecases;

namespace ReportGenerator.Tests.Usecases
{
    [TestFixture(Category = "TestesA")]
    public class ClasseATests
    {
        private ClasseA _classe;

        [SetUp]
        public void SetUp()
        {
            _classe = new ClasseA();
        }

        [Test]
        public void Validar()
        {
            Assert.IsTrue(_classe.Executar());
        }
    }
}
