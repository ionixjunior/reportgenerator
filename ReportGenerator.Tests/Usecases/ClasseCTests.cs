﻿using System;
using NUnit.Framework;
using ReportGenerator.Usecases;

namespace ReportGenerator.Tests.Usecases
{
    [TestFixture(Category = "TestesC")]
    public class ClasseCTests
    {
        private ClasseC _classe;

        [SetUp]
        public void SetUp()
        {
            _classe = new ClasseC();
        }

        [Test]
        public void Validar()
        {
            Assert.IsTrue(_classe.Executar());
        }
    }
}
