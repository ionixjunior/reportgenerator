#!/bin/bash

rm -rf public/*
rm -rf ReportGenerator.Tests/TestResults/*
dotnet test --logger:"junit" --collect:"XPlat Code Coverage" --filter:"TestCategory=TestesA" ReportGenerator.Tests/ReportGenerator.Tests.csproj &&
dotnet test --logger:"junit" --collect:"XPlat Code Coverage" --filter:"TestCategory=TestesB" ReportGenerator.Tests/ReportGenerator.Tests.csproj &&
dotnet test --logger:"junit" --collect:"XPlat Code Coverage" --filter:"TestCategory=TestesC" ReportGenerator.Tests/ReportGenerator.Tests.csproj &&
reportgenerator "-reports:ReportGenerator.Tests/TestResults/*/coverage.cobertura.xml" "-reporttypes:Html;TextSummary" "-targetdir:public" &&
open public/index.html